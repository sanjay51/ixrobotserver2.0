#!/usr/bin/env bash

mkdir grad_temp
cd grad_temp
wget https://services.gradle.org/distributions/gradle-3.5-bin.zip
sudo mkdir /opt/gradle
sudo chmod 777 /opt/gradle
unzip -d /opt/gradle gradle-3.5-bin.zip
export PATH=$PATH:/opt/gradle/gradle-3.5/bin
sudo mv /usr/bin/gradle /usr/bin/gradle-old
sudo ln -sf /opt/gradle/gradle-3.5/bin/gradle /usr/bin/gradle
cd ..
rm -rf grad_temp