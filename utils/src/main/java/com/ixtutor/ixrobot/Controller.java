package com.ixtutor.ixrobot;

import com.ixtutor.ixrobot.physical.motor.BasicMotor;
import com.ixtutor.ixrobot.physical.motor.DoubleMotor;
import com.pi4j.io.gpio.*;
import org.joda.time.Days;
import org.joda.time.LocalDate;

public class Controller {
	static Thread motorThread;

	public int daysToNewYear() {
		if (motorThread == null) {
			GpioFactory.setDefaultProvider(new RaspiGpioProvider(RaspiPinNumberingScheme.DEFAULT_PIN_NUMBERING));
			final GpioController gpio = GpioFactory.getInstance();

			BasicMotor motorLeft = new BasicMotor(gpio,
					RaspiPin.GPIO_07, RaspiPin.GPIO_00, RaspiPin.GPIO_02, RaspiPin.GPIO_03);
			BasicMotor motorRight = new BasicMotor(gpio,
					RaspiPin.GPIO_21, RaspiPin.GPIO_22, RaspiPin.GPIO_23, RaspiPin.GPIO_24);

			DoubleMotor motor = new DoubleMotor(motorLeft, motorRight);
			motor.setState(DoubleMotor.MotorState.FORWARD);

			motorThread = new Thread(motor);
			motorThread.start();
		}

		LocalDate fromDate = new LocalDate();
		LocalDate newYear = fromDate.plusYears(1).withDayOfYear(1);
		return Days.daysBetween(fromDate, newYear).getDays();
	}

}