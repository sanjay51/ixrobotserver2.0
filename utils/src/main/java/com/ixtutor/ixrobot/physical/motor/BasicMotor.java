package com.ixtutor.ixrobot.physical.motor;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;

/**
 * Created by sanjav on 5/20/17.
 */
public class BasicMotor {
    final GpioPinDigitalOutput pin1;
    final GpioPinDigitalOutput pin2;
    final GpioPinDigitalOutput pin3;
    final GpioPinDigitalOutput pin4;

    int pointer = 0;

    static final int[][] Seq = new int[][] {
            {1,0,0,1},
            {1,1,0,0},
            {0,1,0,0},
            {0,1,1,0},
            {0,0,1,0},
            {0,0,1,1},
            {0,0,0,1}
    };

    public BasicMotor(GpioController gpio, Pin pin1, Pin pin2, Pin pin3, Pin pin4) {
        this.pin1 = gpio.provisionDigitalOutputPin(pin1, "Input1", PinState.LOW);
        this.pin2 = gpio.provisionDigitalOutputPin(pin1, "Input2", PinState.LOW);
        this.pin3 = gpio.provisionDigitalOutputPin(pin1, "Input3", PinState.LOW);
        this.pin4 = gpio.provisionDigitalOutputPin(pin1, "Input4", PinState.LOW);

        this.pin1.setShutdownOptions(true, PinState.LOW);
        this.pin2.setShutdownOptions(true, PinState.LOW);
        this.pin3.setShutdownOptions(true, PinState.LOW);
        this.pin4.setShutdownOptions(true, PinState.LOW);
    }

    public void stepClockwise() {
        pointer = (pointer + 1) % 7;
        resetPinStates();
    }

    public void stepAntiClockwise() {
        pointer = (pointer - 1) % 7;
        resetPinStates();
    }

    public void stepLeftForward() {
        this.stepClockwise();
    }

    public void stepRightForward() {
        this.stepAntiClockwise();
    }

    public void stepLeftReverse() {
        this.stepAntiClockwise();
    }

    public void stepRightReverse() {
        this.stepClockwise();
    }

    private void resetPinStates() {
        this.pin1.setState(getPinStateFromInt(Seq[pointer][0]));
        this.pin2.setState(getPinStateFromInt(Seq[pointer][1]));
        this.pin3.setState(getPinStateFromInt(Seq[pointer][2]));
        this.pin4.setState(getPinStateFromInt(Seq[pointer][3]));
    }

    public PinState getPinStateFromInt(int state) {
        return (state == 1 ? PinState.HIGH : PinState.LOW);
    }
}
