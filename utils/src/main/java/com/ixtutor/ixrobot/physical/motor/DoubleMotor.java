package com.ixtutor.ixrobot.physical.motor;

import static com.ixtutor.ixrobot.physical.motor.DoubleMotor.MotorState.STOP;

public class DoubleMotor implements Runnable {
    BasicMotor motorLeft;
    BasicMotor motorRight;
    MotorState state = STOP;

    public DoubleMotor(BasicMotor motorLeft, BasicMotor motorRight) {
        this.motorLeft = motorLeft;
        this.motorRight = motorRight;
    }

    public void forward() {
        this.motorLeft.stepLeftForward();
        this.motorRight.stepRightForward();
    }

    public void reverse() {
        this.motorLeft.stepLeftReverse();
        this.motorRight.stepRightReverse();
    }

    public void takeLeftForward() {
        this.motorLeft.stepLeftForward();
    }

    public void takeLeftReverse() {
        this.motorRight.stepLeftReverse();
    }

    public void takeRightForward() {
        this.motorRight.stepRightForward();
    }

    public void takeRightReverse() {
        this.motorLeft.stepRightReverse();
    }

    public void setState(MotorState motorState) {
        this.state = motorState;
    }

    @Override
    public void run() {
        while (true) {
            this.sleepForSometime();

            switch (this.state) {
                case LEFT_FORWARD:
                    this.takeLeftForward();
                    break;
                case LEFT_REVERSE:
                    this.takeLeftReverse();
                    break;
                case RIGHT_FORWARD:
                    this.takeRightForward();
                    break;
                case RIGHT_REVERSE:
                    this.takeRightReverse();
                    break;
                case FORWARD:
                    this.forward();
                    break;
                case REVERSE:
                    this.reverse();
                    break;
                default:
                    continue;
            }
        }
    }

    private void sleepForSometime() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            System.out.println("[WARN] Motor sleep interrupted.");
        }
    }

    public enum MotorState { STOP,
        FORWARD, REVERSE,
        LEFT_FORWARD, LEFT_REVERSE,
        RIGHT_FORWARD, RIGHT_REVERSE }
}
